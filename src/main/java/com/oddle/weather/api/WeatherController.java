package com.oddle.weather.api;

import com.oddle.weather.model.dto.PageDto;
import com.oddle.weather.model.dto.WeatherForecastDto;
import com.oddle.weather.service.WeatherForecastService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/weather")
public class WeatherController {

    private static final Logger LOG = LoggerFactory.getLogger(WeatherController.class);

    @Autowired
    private WeatherForecastService weatherForecastService;

    @RequestMapping("/{city}")
    public ResponseEntity<PageDto<WeatherForecastDto>> getWeather(@PathVariable("city") String city, Pageable pageable) {
        LOG.info("Get Weather Forecast for {} City", city);
        return new ResponseEntity<>(weatherForecastService.findByCityName(city, pageable), HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public ResponseEntity deleteForecast(@PathVariable("id") Long id) {
        LOG.info("Delete Weather Forecast with ID of {}", id);
        weatherForecastService.deleteForecast(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
