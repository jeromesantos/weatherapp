package com.oddle.weather.repository;

import com.oddle.weather.model.entity.WeatherForecast;
import org.joda.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WeatherForecastRepository extends JpaRepository<WeatherForecast, Long> {

    Page<WeatherForecast> findByCityName(String name, Pageable pageable);

    WeatherForecast findByCityNameAndDate(String name, LocalDate date);
}
