package com.oddle.weather.repository;


import com.oddle.weather.model.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Long> {

    City findByNameIgnoreCase(String name);

}
