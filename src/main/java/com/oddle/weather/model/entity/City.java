package com.oddle.weather.model.entity;

import com.google.common.collect.Sets;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "CITY")
public class City extends BaseModel {

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private Set<WeatherForecast> weatherForecasts = Sets.newHashSet();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<WeatherForecast> getWeatherForecasts() {
        return weatherForecasts;
    }

    public void setWeatherForecasts(Set<WeatherForecast> weatherForecasts) {
        this.weatherForecasts = weatherForecasts;
    }
}
