package com.oddle.weather.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

public class WeatherForecastDto {

    private Long id;

    private String cityName;

    private Double humidity;

    private Double pressure;

    private Double windSpeed;

    private String description;

    private Double temperature;

    private String date;

    public String getCityName() {
        return cityName;
    }

    @JsonSetter("name")
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(Double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    @JsonProperty("weather")
    public void setWeather(List<Map<String, Object>> weatherEntries) {
        if (!CollectionUtils.isEmpty(weatherEntries)) {
            setDescription(weatherEntries.get(0).get("main").toString());
        }
    }

    @JsonProperty("main")
    public void setMainEntries(Map<String, Object> main) {
        setHumidity(Double.parseDouble(main.get("humidity").toString()));
        setPressure(Double.parseDouble(main.get("pressure").toString()));
        setTemperature(Double.parseDouble(main.get("temp").toString()));
    }

    @JsonProperty("wind")
    public void setWind(Map<String, Object> wind) {
        setWindSpeed(Double.parseDouble(wind.get("speed").toString()));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
