package com.oddle.weather.model.dto;

import com.google.common.collect.Lists;
import java.util.List;

public class CityDto {

    private Long id;

    private String name;

    private List<WeatherForecastDto> weatherForecasts = Lists.newArrayList();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<WeatherForecastDto> getWeatherForecasts() {
        return weatherForecasts;
    }

    public void setWeatherForecasts(List<WeatherForecastDto> weatherForecasts) {
        this.weatherForecasts = weatherForecasts;
    }
}
