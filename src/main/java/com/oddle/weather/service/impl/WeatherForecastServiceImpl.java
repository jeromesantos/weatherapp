package com.oddle.weather.service.impl;

import com.google.common.collect.Lists;
import com.oddle.weather.model.dto.PageDto;
import com.oddle.weather.model.dto.WeatherForecastDto;
import com.oddle.weather.model.entity.City;
import com.oddle.weather.model.entity.WeatherForecast;
import com.oddle.weather.repository.CityRepository;
import com.oddle.weather.repository.WeatherForecastRepository;
import com.oddle.weather.service.WeatherForecastService;
import com.oddle.weather.service.mapper.WeatherCustomMapper;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import javax.transaction.Transactional;
import java.net.URI;
import java.util.stream.Collectors;

@Service
public class WeatherForecastServiceImpl implements WeatherForecastService {

    private static final Logger LOG = LoggerFactory.getLogger(WeatherForecastServiceImpl.class);

    private static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?q={city},&appid={key}";

    @Value("${owp.key}")
    private String key;

    @Autowired
    private WeatherForecastRepository weatherForecastRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private WeatherCustomMapper mapper;

    @Autowired
    private RestTemplate restTemplate;

    @Transactional
    @Override
    public PageDto<WeatherForecastDto> findByCityName(String name, Pageable pageable) {
        City city = cityRepository.findByNameIgnoreCase(name);
        Page<WeatherForecast> weatherForecastPage = new PageImpl<>(Lists.newArrayList());

        try {
            if (city != null) {
                WeatherForecast forecastToday = weatherForecastRepository.findByCityNameAndDate(city.getName(), LocalDate.now());

                if (forecastToday == null) {
                    WeatherForecast latestForecast = mapper.map(getLatestForecast(city.getName()), WeatherForecast.class);
                    latestForecast.setCity(city);
                    latestForecast.setDate(LocalDate.now());
                    weatherForecastRepository.save(latestForecast);
                }
            } else {
                WeatherForecastDto weatherForecastDto = getLatestForecast(name);
                city = new City();
                city.setName(weatherForecastDto.getCityName());
                city = cityRepository.save(city);

                WeatherForecast weatherForecast = mapper.map(weatherForecastDto, WeatherForecast.class);
                weatherForecast.setCity(city);
                weatherForecast.setDate(LocalDate.now());

                weatherForecastRepository.save(weatherForecast);
            }
            weatherForecastPage = weatherForecastRepository.findByCityName(city.getName(), pageable);
        } catch (HttpClientErrorException e) {
            LOG.error("CITY DOES NOT EXISTS");
        }

        return PageDto.newPageInfo(weatherForecastPage,
                weatherForecastPage.getContent()
                        .stream()
                        .map(weatherForecast -> mapper.map(weatherForecast, WeatherForecastDto.class))
                        .collect(Collectors.toList()));
    }

    @Transactional
    public void deleteForecast(Long id) {
        weatherForecastRepository.delete(id);
    }

    private WeatherForecastDto getLatestForecast(String city) throws HttpClientErrorException {
        URI url = new UriTemplate(WEATHER_URL).expand(city, this.key);

        return restTemplate.getForEntity(url, WeatherForecastDto.class).getBody();
    }
}
