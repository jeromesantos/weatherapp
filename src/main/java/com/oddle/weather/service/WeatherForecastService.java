package com.oddle.weather.service;

import com.oddle.weather.model.dto.PageDto;
import com.oddle.weather.model.dto.WeatherForecastDto;
import org.springframework.data.domain.Pageable;

public interface WeatherForecastService {

    PageDto<WeatherForecastDto> findByCityName(String name, Pageable pageable);

    void deleteForecast(Long id);
}
