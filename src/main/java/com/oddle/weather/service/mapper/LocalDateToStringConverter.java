package com.oddle.weather.service.mapper;

import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

@Component
public class LocalDateToStringConverter extends BidirectionalConverter<LocalDate, String> {

    public static final String DATE = "MMM-dd-yyyy";

    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern(DATE);

    @Override
    public String convertTo(LocalDate localDate, Type<String> type) {
        return localDate.toString(DATE);
    }

    @Override
    public LocalDate convertFrom(String s, Type<LocalDate> type) {
        return DATE_FORMATTER.parseLocalDate(s);
    }
}
