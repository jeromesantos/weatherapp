package com.oddle.weather.service.mapper;


import com.oddle.weather.model.dto.WeatherForecastDto;
import com.oddle.weather.model.entity.WeatherForecast;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class WeatherCustomMapper extends ConfigurableMapper {


    @Override
    public void configure(MapperFactory factory) {
        factory.getConverterFactory().registerConverter(new LocalDateToStringConverter());

        factory.classMap(WeatherForecast.class, WeatherForecastDto.class)
                .fieldAToB("city.name", "cityName")
                .byDefault()
                .register();
    }
}

