(function () {
  'use strict';

  angular
    .module('weatherApp')
    .controller('WeatherController', WeatherController);

    WeatherController.$inject = ['WeatherService', 'NgTableParams'];

    function WeatherController(WeatherService, NgTableParams) {
      var vm = this;
      vm.city = "";
      vm.deleteForecast = deleteForecast;
      vm.search = search;

      vm.weatherTable = new NgTableParams({page: 1, count: 10}, {
        getData: function (params) {
          return WeatherService.findByCity(vm.city, {
            page: params.page() - 1,
            size: params.count(),
            sort: 'date,asc'
          }).then(function (data) {
            params.total(data.totalElements);
            return data.results;
          });
        }
      });

      function deleteForecast(id) {
        WeatherService.deleteForecast(id).then(function() {
          console.log("length", vm.weatherTable.data.length);
          if (vm.weatherTable.data.length == 1) {
            vm.city = "";
          }
          reloadTable();
        });
      }

      function search() {
        reloadTable();
      }

      function reloadTable() {
        vm.weatherTable.page(1)
        vm.weatherTable.reload();
      }
    }

})();
