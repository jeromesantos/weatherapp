(function () {
  'use strict';

  angular.module('weatherApp')
    .factory('WeatherService', WeatherService);

  WeatherService.$inject = ['$http', '$q'];
  function WeatherService($http, $q) {
    return {
      findByCity: findByCity,
      deleteForecast: deleteForecast
    };

    function findByCity(city, params) {
      city = city == "" ? " " : city;
      var deferred = $q.defer();
      $http.get('/api/weather/' + city, {
        params: params
      }).success(function(data) {
        deferred.resolve(data);
      });

      return deferred.promise;
    }

    function deleteForecast(id) {
      return $http.post('/api/weather/delete/'+ id);
    }

  }
})();
